# res_reader

## Description
Python scripts that should help me to read benchmarks results from a txt file
to process them and to make some nice plots

In `plots/my_data_frames.py` the relevant classes are:

- `Meta_Measure`
- `Data_Measure`

There you can specify in what data you are interested.
Here an examples

```Python
self.loop_iters: Member[int] = Member(int, 0, "loop_iters: ")
```

You define a member variable with any name and initialize it with a `Member` object.
The Member object consists of 3 parameters which you have to pass during initialization.

These are:

1. A parsing function that transforms a string with the data in it to your desired data type. \
If you string contains for example just an integer or float e.g. `"7"` or `"3.14"` then the \
pythons type classes are already enough.\
Else give it a function receiving a string and returning what ever data type you want.

2. A default value

3. A string that identifies the line in the txt file which contains the corresponding data.

    For example the file contains a line with `"loop_iters: 7000\n"` in it then you could put \
    `"loop_iters: "` as the identifier string. The rest of the line (excluding the line break) is then forwarded to your parser function.

An usage example is given in `plots/path.py`