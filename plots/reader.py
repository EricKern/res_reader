from dataclasses import dataclass
from typing import List, Callable, TypeVar, Generic
import enum
import os


T = TypeVar('T')

@dataclass
class Member(Generic[T]):

    val_parser: Callable[[str], T]
    """Function transforming data string to the desired type
    Could be for example just int("13") if T is int""" 
    
    value: T
    """Actual value of that member""" 

    id: str
    """Idenitifier string to look for in the file to find the member value""" 
    
# Adapt below -----------------------------------------------------------------

class Meta_Measure:
    """Class holding meta information for measurements"""
    def __init__(self) -> None:
        self.NodeList:       Member[str] = Member(str, "", "NodeList: ")
        self.Nodes:          Member[int] = Member(int, 0, "Nodes: ")
        self.Tasks_per_Node: Member[int] = Member(int, 0, "Tasks-per-Node: ")
        self.loop_iters:     Member[int] = Member(int, 0, "loop_iters: ")
        self.N_proc:         Member[int] = Member(int, 0, "N_proc: ")

def remove_s(string: str) -> float:
    """Parser for time measurements removes trailing s (Seconds)
    from strings"""
    return float(string[:-1])

class Data_Measure:
    """Class holding meta information for measurements"""
    def __init__(self) -> None:
        self.t_my_barrier:  Member[float] = Member(remove_s, 0.0, " My barrier took: ")
        self.t_mpi_barrier: Member[float] = Member(remove_s, 0.0, "MPI barrier took: ")

# Adapt above -----------------------------------------------------------------




class Measure_Factory:

    def __init__(self, file_path: str) -> None:
        self.file_path: str = file_path
        self.experiment_meta: Meta_Measure = Meta_Measure()
        self.experiment_data: List[Data_Measure] = [Data_Measure()]
        self.meta_progress: int = 0
        self.data_progress: int = 0
        self.n_meta = len(vars(Meta_Measure())) # gives # of fields
        self.n_data = len(vars(Data_Measure()))

    @property
    def file_path(self):
        return self._file_path

    @file_path.setter
    def file_path(self, f_path: str):
        if os.path.isfile(f_path):
            self._file_path = f_path  # makes private variable outside __init__
        else:
            raise ValueError("Passed path seems to be no valid path")

    def read(self):
        ReadStates = enum.Enum('ReadStates', ['readingMeta', 'readingData'])

        with open(self.file_path, "rt") as f:
            curr_state = ReadStates.readingMeta
            for line in f:  # iterate over lines
                # Line Pre Processing-------------------
                if line.isspace():  # continue if empty line
                    continue
                if line.startswith("End-Meta-Info"):
                    curr_state=ReadStates.readingData
                # ----------------------------------------

                if curr_state == ReadStates.readingMeta:
                    self.make_Meta_Measure(line)
                elif curr_state == ReadStates.readingData:
                    self.make_Data_Measure(line)
                else:
                    raise NotImplementedError

    def make_Meta_Measure(self, line: str):
        success = self.fill_data_cls(line, self.experiment_meta)
        if success:
            self.meta_progress += 1

    def make_Data_Measure(self, line: str):
        if self.data_progress == self.n_data:
            self.experiment_data.append(Data_Measure())
            self.data_progress = 0

        success = self.fill_data_cls(line, self.experiment_data[-1])
        if success:
            self.data_progress += 1

    def fill_data_cls(self, line: str, data_cls: Meta_Measure|Data_Measure):
        # check for every member of the data class if line starts
        # with corresponding identifier
        # Maybe not best performance but least constraints on file format
        for attr_name, member_obj in vars(data_cls).items():
            id = member_obj.id
            # get substring to search for

            if line.startswith(id):
                # if found cast remaining string to type and assign
                member_obj.value = member_obj.val_parser(line[len(id):-1])
                return True
            else:
                continue
        return False # no matching attribute found


    def get_meta(self) -> Meta_Measure:
        return self.experiment_meta

    def get_data(self) -> List[Data_Measure]:
        return self.experiment_data