import os
from typing import List
import numpy as np
import matplotlib.pyplot as plt
# plt.rcParams.update({'font.size': 11})

from reader import Measure_Factory


# can be used to find multiple files
def get_measurement_files() -> List[str]:
    m_files = []
    for root, dirs, files in os.walk("../measurements", topdown=True):
        for name in files:
            m_files.append(os.path.join(root, name))
        break # scan only top lvl directory
    return m_files


if __name__ == "__main__":
    files = get_measurement_files()
    factories: List[Measure_Factory] = [None]*len(files)

    for i, file in enumerate(files) :
        factories[i] = Measure_Factory(file)
        factories[i].read()

    meta_info = factories[0].get_meta()
    data_points = factories[0].get_data()

    meta_info.loop_iters.val_parser

    times1 = map(lambda p : p.t_my_barrier.value, data_points)
    times2 = map(lambda p : p.t_mpi_barrier.value, data_points)

    t_my_barrier_vals = np.fromiter(times1, dtype=np.float64)
    t_mpi_barrier_vals = np.fromiter(times2, dtype=np.float64)

    n_iter = meta_info.loop_iters.value
    node_name = meta_info.NodeList.value
    n_proc = meta_info.N_proc.value

    plt.plot(t_my_barrier_vals, marker='o', label='Own Barrier')
    plt.plot(t_mpi_barrier_vals, marker='o', label='MPI_Barrier')
    plt.ylabel("$T_{\mathsf{average}}$ in s")
    plt.xlabel("Iteration")

    plt.title("Barriers on " + node_name + " with " + str(n_proc) + " processes")
    plt.legend()

    plt.show()
    # plt.savefig("result.pdf", bbox_inches='tight')

    debug = 1